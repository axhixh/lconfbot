-- Initialisation script.
-- 
-- This script runs once to completion on startup before the IM layer
-- initialises.

function include(fn)
	local c, e = loadfile(fn)
	if e then
		error(e)
	end
	c()
end

include("settings.lua")
include("conversation.lua")

-- Scheduler and external entry points.

local conversations = {}

purple.conversation_start_cb = function(cv)
	local c = Conversation.new(cv)
	conversations[cv] = c
	c:resume()
end

purple.conversation_stop_cb = function(cv)
	conversations[cv] = nil
end

purple.incoming_cb = function(cv, name, alias, message, time)
	local c = conversations[cv]
	if not c then
		-- The conversation was destroyed with an event pending.
		return
	end
	
	c:resume(name, alias, message, time)
end

purple.timeout_cb = function(cv, name, message, time)
	local c = conversations[cv]
	if not c then
		-- The conversation was destroyed with an event pending.
		return
	end
	
	c:resume()
end

purple.buddy_status_changed = function(buddy, status)
    local message = "CONFBOT" .. ': '
    if status == 1 then
        message = message .. buddy .. ' has signed on'
    elseif status == 2 then
        message = message .. buddy .. ' has signed off'
    elseif status == 3 then
        message = message .. buddy .. ' is available'
    elseif status == 4 then
        message = message .. buddy .. ' is unavailable'
    end

    if status == 1 or status == 2 then
    	purple.broadcast(buddy, message, 1)
    	purple.log(message)
    end
end

purple.log = function(message)
	local filename = os.date(LOG_FILE_NAME, os.time())
	logfile, err_msg = io.open(filename,'a')
	if logfile then
	    logfile:write(os.date())
	    logfile:write(", ")
	    logfile:write(message)
	    logfile:write('\n')
	    logfile:close()
	else
	    io.write('Unable to open log file. ' .. err_msg)
	end
end

