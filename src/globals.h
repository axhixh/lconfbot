#ifndef GLOBALS_H
#define GLOBALS_H

/* Settings */

#define STATUS "Bored..."
#define UI_ID "purple"

#define STATUS_SIGNED_ON 1
#define STATUS_SIGNED_OFF 2
#define STATUS_AVAILABLE 3
#define STATUS_UNAVAILABLE 4

/* IM interface */

typedef struct Conversation Conversation;

extern void im_init(void);
extern void im_mainloop(void);
extern void im_terminate_mainloop(void);
extern void im_deinit(void);

extern void im_conversation_start_cb(Conversation* cv);
extern void im_conversation_stop_cb(Conversation* cv);
extern void im_incoming_cb(Conversation* cv, const char* name, const char* alias, const char* message, time_t mtime);
extern void im_timeout_cb(Conversation* cv);

extern void im_reply(Conversation* cv, const char* message);
extern int im_add_timeout(Conversation* cv, unsigned int interval);
extern void im_remove_timeout(Conversation* cv, unsigned int id);

extern void broadcast(const char* name, const char* message, int status);
extern void send_list(Conversation* cv, unsigned int only_online);
extern void set_status(Conversation* cv, const char* status);

/* Scripting system */

extern void script_init(void);
extern void script_load(const char* filename);
extern void script_deinit(void);

extern void script_notify_conversation_start(Conversation* cv);
extern void script_notify_conversation_stop(Conversation* cv);
extern void script_notify_incoming(Conversation* cv, const char* name, const char* alias, const char* message, time_t mtime);
extern void script_notify_timeout(Conversation* cv);
extern void script_notify_buddy_status_changed(const char *buddy_name, unsigned int buddy_status);
#endif
