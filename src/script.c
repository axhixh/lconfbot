#include <stdlib.h>
#include <stdio.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include "globals.h"

static lua_State* L = NULL;
 
static int report(lua_State* L, int status)
{
	if (status && !lua_isnil(L, -1))
	{
		const char* msg = lua_tostring(L, -1);
		if (!msg)
			msg = "(error object is not a string)";
		fprintf(stderr, "Lua error: %s\n", msg);
		lua_pop(L, 1);
		
		exit(1);
	}
	
	return status;
}

static int traceback (lua_State *L)
{
	lua_getfield(L, LUA_GLOBALSINDEX, "debug");
	if (!lua_istable(L, -1))
	{
		lua_pop(L, 1);
		return 1;
	}
	
	lua_getfield(L, -1, "traceback");
	if (!lua_isfunction(L, -1))
	{
		lua_pop(L, 2);
		return 1;
	}
	
	lua_pushvalue(L, 1);  /* pass error message */
	lua_pushinteger(L, 2);  /* skip this function and traceback */
	lua_call(L, 2, 1);  /* call debug.traceback */
	return 1;
}

static int docall(lua_State* L, int narg, int clear)
{
	int base = lua_gettop(L) - narg;
	lua_pushcfunction(L, traceback);
	lua_insert(L, base);
	
	int status = lua_pcall(L, narg, (clear ? 0 : LUA_MULTRET), base);
	
	lua_remove(L, base);
	
	if (status != 0)
		lua_gc(L, LUA_GCCOLLECT, 0);
	return status;
}

static int broadcast_cb(lua_State* L)
{
	const char* name = luaL_checkstring(L, 1);
	const char* message = luaL_checkstring(L, 2);
	int is_status = luaL_checknumber(L, 3);

	broadcast(name, message, is_status);
	return 0;
}
static int send_list_cb(lua_State* L)
{
	Conversation* cv = (Conversation*) lua_touserdata(L, 1);
	unsigned int only_online = luaL_checknumber(L, 2);
	send_list(cv, only_online);
	return 0;
}

static int reply_cb(lua_State* L)
{
	Conversation* cv = (Conversation*) lua_touserdata(L, 1);
	const char* message = luaL_checkstring(L, 2);
	
	im_reply(cv, message);
	
	return 0;
}

static int add_timeout_cb(lua_State* L)
{
	Conversation* cv = (Conversation*) lua_touserdata(L, 1);
	int delay = luaL_checknumber(L, 2);
	int id = im_add_timeout(cv, delay);
	lua_pushinteger(L, id);
	return 1;
}

static int remove_timeout_cb(lua_State* L)
{
	Conversation* cv = (Conversation*) lua_touserdata(L, 1);
	int id = luaL_checknumber(L, 2);
	im_remove_timeout(cv, id);
	return 0;
}

static int set_status_cb(lua_State* L)
{
	Conversation* cv = (Conversation*) lua_touserdata(L, 1);
	const char* status = luaL_checkstring(L, 2);

	set_status(cv, status);

	return 0;
}

void script_init(void)
{
	L = lua_open();
	luaL_openlibs(L);
	
	const static luaL_Reg funcs[] =
	{
		{ "reply",           reply_cb },
		{ "add_timeout",     add_timeout_cb },
		{ "remove_timeout",  remove_timeout_cb },
		{ "broadcast",       broadcast_cb},
		{ "send_list",       send_list_cb},
		{ "set_status",      set_status_cb},
		{ NULL,              NULL }
	};
	
	luaL_register(L, "purple", funcs);
}

void script_load(const char* filename)
{
	int status = luaL_loadfile(L, filename);
	status = status || docall(L, 0, 1);
	(void) report(L, status);
}

void script_deinit(void)
{
	lua_close(L);
}

void script_notify_conversation_start(Conversation* cv)
{
	lua_getglobal(L, "purple");
	lua_getfield(L, -1, "conversation_start_cb");
	lua_pushlightuserdata(L, cv);
	
	int status = docall(L, 1, 0);
	(void) report(L, status);
}

void script_notify_conversation_stop(Conversation* cv)
{
	lua_getglobal(L, "purple");
	lua_getfield(L, -1, "conversation_stop_cb");
	lua_pushlightuserdata(L, cv);
	
	int status = docall(L, 1, 0);
	(void) report(L, status);
}

void script_notify_incoming(Conversation* cv, const char* name, const char* alias, const char* message, time_t mtime)
{
	lua_getglobal(L, "purple");
	lua_getfield(L, -1, "incoming_cb");
	lua_pushlightuserdata(L, cv);
	lua_pushstring(L, name);
	lua_pushstring(L, alias);
	lua_pushstring(L, message);
	lua_pushnumber(L, mtime);
	
	int status = docall(L, 5, 0);
	(void) report(L, status);
}

void script_notify_timeout(Conversation* cv)
{
	lua_getglobal(L, "purple");
	lua_getfield(L, -1, "timeout_cb");
	lua_pushlightuserdata(L, cv);
	
	int status = docall(L, 1, 0);
	(void) report(L, status);
}

void script_notify_buddy_status_changed(const char *buddy_name, unsigned int buddy_status)
{
    lua_getglobal(L, "purple");
    lua_getfield(L, -1, "buddy_status_changed");
    lua_pushstring(L, buddy_name);
    lua_pushnumber(L, buddy_status);

    int status = docall(L, 2, 0);
    (void) report(L, status);
}

