#include <stdlib.h>
#include <stdio.h>
#include "globals.h"

void im_conversation_start_cb(Conversation* cv)
{
	script_notify_conversation_start(cv);
}

void im_conversation_stop_cb(Conversation* cv)
{
	script_notify_conversation_stop(cv);
}

void im_incoming_cb(Conversation* cv, const char* name, const char* alias, const char* message,
	time_t mtime)
{
	script_notify_incoming(cv, name, alias, message, mtime);
}

void im_timeout_cb(Conversation* cv)
{
	script_notify_timeout(cv);
}

int main(int argc, const char* argv[])
{
	script_init();
	script_load("init.lua");
	
	im_init();
	im_mainloop();
	im_deinit();
	
	script_deinit();
	
	return 0;
}
