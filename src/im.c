#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <prpl.h>
#include <debug.h> // purple
#include <core.h> // purple
#include <savedstatuses.h> // purple
#include <idle.h> // purple
#include <conversation.h> // purple
#include <blist.h> // purple
#include <status.h> // purple
#include "globals.h"

#define INITIAL_RECON_DELAY_MIN 8000
#define INITIAL_RECON_DELAY_MAX 60000
#define MAX_RECON_DELAY 600000

/* --- CoreUiOps --------------------------------------------------------- */
static GHashTable* melissa_ui_get_info()
{
	static GHashTable* ui_info = NULL;

	if (!ui_info)
	{
		ui_info = g_hash_table_new(g_str_hash, g_str_equal);
		
		g_hash_table_insert(ui_info, "name", "Lua Conference Bot");
		g_hash_table_insert(ui_info, "version", "0.3");
	}
	
	return ui_info;
}

static PurpleCoreUiOps core_ui_ops_table =
{
	ui_prefs_init: NULL,
	debug_ui_init: NULL, // default provided
	ui_init: NULL,
	quit: NULL,
	get_ui_info: melissa_ui_get_info,
};

/* --- EventLoopUiOps ---------------------------------------------------- */

typedef struct _MelissaIOClosure
{
        PurpleInputFunction function;
        guint result;
        gpointer data;
} MelissaIOClosure;

#define MELISSA_READ_COND (G_IO_IN | G_IO_HUP | G_IO_ERR)
#define MELISSA_WRITE_COND (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)
  
static void melissa_io_destroy(gpointer data)
{
	g_free(data);
}

static gboolean melissa_io_invoke(GIOChannel *source, GIOCondition condition,
	gpointer data)
{
	MelissaIOClosure *closure = data;
	PurpleInputCondition purple_cond = 0;
	
	if (condition & MELISSA_READ_COND)
		purple_cond |= PURPLE_INPUT_READ;
	if (condition & MELISSA_WRITE_COND)
		purple_cond |= PURPLE_INPUT_WRITE;
	
#if 0
    purple_debug(PURPLE_DEBUG_MISC, "gtk_eventloop",
		"CLOSURE: callback for %d, fd is %d\n",
		closure->result, g_io_channel_unix_get_fd(source));
#endif

	closure->function(closure->data, g_io_channel_unix_get_fd(source),
		purple_cond);
	
	return TRUE;
}


static guint melissa_input_add(gint fd, PurpleInputCondition condition,
		PurpleInputFunction function, gpointer data)
{
	MelissaIOClosure *closure = g_new0(MelissaIOClosure, 1);
	GIOChannel *channel;
	GIOCondition cond = 0;
	
	closure->function = function;
	closure->data = data;
	
	if (condition & PURPLE_INPUT_READ)
		cond |= MELISSA_READ_COND;
	if (condition & PURPLE_INPUT_WRITE)
		cond |= MELISSA_WRITE_COND;
	
	channel = g_io_channel_unix_new(fd);
	closure->result = g_io_add_watch_full(channel, G_PRIORITY_DEFAULT, cond,
		melissa_io_invoke, closure, melissa_io_destroy);

	g_io_channel_unref(channel);
	return closure->result;
}

static PurpleEventLoopUiOps eventloop_ui_ops_table =
{
	timeout_add: g_timeout_add,
	timeout_remove: g_source_remove,
	input_add: melissa_input_add,
	input_remove: g_source_remove,
};
/* --- AccountUiOps ------------------------------------------------------ */
static void *account_request_authorize(PurpleAccount *account, const char *remote_user, const char *id,
        const char *alias, const char *message, gboolean on_list, PurpleAccountRequestAuthorizationCb authorize_cb,
        PurpleAccountRequestAuthorizationCb deny_cb, void *user_data)
{
    authorize_cb(user_data);
    if (!on_list) {
        purple_blist_request_add_buddy(account, remote_user, NULL, alias);
    }
    return NULL;
}
static void account_request_close(void *handle)
{
}

static PurpleAccountUiOps account_ui_ops_table =
{
    request_authorize: account_request_authorize,
    close_account_request: account_request_close
};
/* --- ConnectionUiOps --------------------------------------------------- */
typedef struct {
    int delay;
    guint timeout;
} LconfbotAutoRecon;

static GHashTable *hash = NULL;

static void free_auto_recon(gpointer data)
{
    LconfbotAutoRecon *info = data;
    if (info->timeout != 0)
        g_source_remove(info->timeout);
    g_free(info);
}

static gboolean do_signon(gpointer data)
{
    PurpleAccount *account = data;
    g_return_val_if_fail(account != NULL, FALSE);
    LconfbotAutoRecon *info = g_hash_table_lookup(hash, account);
    if (info) info->timeout = 0;
    PurpleStatus *status = purple_account_get_active_status(account);
    if (purple_status_is_online(status)) {
        purple_account_connect(account);
    }
    return FALSE;
}

static void melissa_connection_report_disconnect(PurpleConnection *gc, PurpleConnectionError reason, const char *text)
{
    PurpleAccount *account = purple_connection_get_account(gc);
    if (!purple_connection_error_is_fatal(reason)) {
        LconfbotAutoRecon *info = g_hash_table_lookup(hash, account);
        if (info == NULL) {
            info = g_new0(LconfbotAutoRecon, 1);
            g_hash_table_insert(hash, account, info);
            info->delay = g_random_int_range(INITIAL_RECON_DELAY_MIN, INITIAL_RECON_DELAY_MAX);
        } else {
            info->delay = MIN(2 * info->delay, MAX_RECON_DELAY);
            if (info->timeout != 0)
                g_source_remove(info->timeout);
        }
        info->timeout = g_timeout_add(info->delay, do_signon, account);

    } else {
	    fprintf(stderr, "DISCONNECTED %s!\n", text);
    }
}

static PurpleConnectionUiOps connection_ui_ops_table =
{
	report_disconnect_reason: melissa_connection_report_disconnect
};

/* --- ConversationUiOps ------------------------------------------------- */

static void melissa_create_conversation(PurpleConversation* conv)
{
	im_conversation_start_cb((Conversation*) conv);
}

static void melissa_destroy_conversation(PurpleConversation* conv)
{
	im_conversation_stop_cb((Conversation*) conv);
}

void im_reply(Conversation* cv, const char* text)
{
	PurpleConversation* conv = (PurpleConversation*) cv;
	
	char *escape = g_markup_escape_text(text, -1);
	char *apos = purple_strreplace(escape, "&apos;", "'");
	g_free(escape);
	escape = apos;
	
	switch (purple_conversation_get_type(conv))
	{
		case PURPLE_CONV_TYPE_IM:
			purple_conv_im_send_with_flags(PURPLE_CONV_IM(conv), escape, PURPLE_MESSAGE_SEND);
            break;
            
        case PURPLE_CONV_TYPE_CHAT:
            purple_conv_chat_send(PURPLE_CONV_CHAT(conv), escape);
            break;
            
        default:
        	break;
    }
    g_free(escape);
    purple_idle_touch();
}

static void melissa_write_conversation(PurpleConversation *conv, const char *who,
	const char *alias, const char *message, PurpleMessageFlags flags, time_t mtime)
{
	if (!(flags & PURPLE_MESSAGE_RECV))
		return;
		
	im_incoming_cb((Conversation*) conv, who, alias,  message, mtime);
}

static PurpleConversationUiOps conversation_ui_ops_table =
{
	create_conversation: melissa_create_conversation,
	destroy_conversation: melissa_destroy_conversation,
	write_conv: melissa_write_conversation,
	present: NULL
};

/* --- Timeout management ------------------------------------------------ */

static gboolean timeout_cb(gpointer data)
{
	im_timeout_cb((Conversation*) data);
	return FALSE;
}

int im_add_timeout(Conversation* cv, unsigned int delay)
{
	return g_timeout_add(delay, timeout_cb, (void*) cv);
}

void im_remove_timeout(Conversation* cv, unsigned int id)
{
	GSource* source = g_main_context_find_source_by_id(NULL, id);
	if (!source)
		return;
		
	g_source_destroy(source);
}

void send_list(Conversation* cv, unsigned int only_online)
{
	PurpleConversation* conv = (PurpleConversation*) cv;
	if (purple_conversation_get_type(conv) == PURPLE_CONV_TYPE_IM)
	{
		PurpleBlistNode *root = purple_blist_get_root();
		PurpleBlistNode *buddy = purple_blist_node_next(root, FALSE);
		char *users = only_online > 0 ? "Online -> " : "Users -> ";
		while (buddy)
		{
			if (buddy->type == PURPLE_BLIST_BUDDY_NODE)
			{
				const char *name = purple_buddy_get_name((PurpleBuddy*)buddy);
				PurplePresence *presence = purple_buddy_get_presence((PurpleBuddy*)buddy);
				if (purple_presence_is_available(presence))
				{
					users = g_strconcat(users, "[", name, "] ", NULL);
				}
				else if (!only_online)
				{
					users = g_strconcat(users, name, " ",  NULL);
				}
			}
        		buddy = purple_blist_node_next(buddy, FALSE);
		}
		purple_conv_im_send_with_flags(PURPLE_CONV_IM(conv), users, PURPLE_MESSAGE_SEND);
		g_free(users);
    	}
    	purple_idle_touch();
}

void broadcast(const char* sender_name, const char *text, int is_status)
{
    PurpleBlistNode *root = purple_blist_get_root();
    PurpleBlistNode *buddy = purple_blist_node_next(root, FALSE);
    while (buddy)
    {
	if (buddy->type == PURPLE_BLIST_BUDDY_NODE)
	{
		const char *name = purple_buddy_get_name((PurpleBuddy*)buddy);
		PurplePresence *presence = purple_buddy_get_presence((PurpleBuddy*)buddy);
		if (purple_presence_is_available(presence))
		{
                        int send = 1;
			const char *status = purple_blist_node_get_string(buddy, "notify");
			if (status)
			{
				if (g_strcmp0(status, "none") == 0)
				{
					send = 0;
				}
				else if (is_status == 1 && g_strcmp0(status, "message") == 0)
				{
					send = 0;
				}
				else if (is_status == 0 && g_strcmp0(status, "status") == 0)
				{
					send = 0;
				}
			}

			if (send == 1 && g_strcmp0(sender_name, name) != 0)
			{
				PurpleAccount *account = purple_buddy_get_account((PurpleBuddy*)buddy);
				PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM, name, account);
				if (conv == NULL)
				{
					conv = purple_conversation_new(PURPLE_CONV_TYPE_IM, account, name);
				}
				purple_conv_im_send_with_flags(PURPLE_CONV_IM(conv), text, PURPLE_MESSAGE_SEND);
			}
		}
	}
        buddy = purple_blist_node_next(buddy, FALSE);
    }
}

static PurpleBlistNode* get_buddy(PurpleConversation* conversation)
{
	const char *conv_name = conversation->name;
	int len_conv = strlen(conv_name);
	PurpleBlistNode *root = purple_blist_get_root();
	PurpleBlistNode *buddy = purple_blist_node_next(root, FALSE);
	while (buddy)
	{
		if (buddy->type == PURPLE_BLIST_BUDDY_NODE)
		{
			const char *name = purple_buddy_get_name((PurpleBuddy*) buddy);
			int len_name = strlen(name);
			if (len_conv >= len_name && 
			    strncmp(conv_name, name, len_name) == 0)
			{
				return buddy;
			}
		}
		buddy = purple_blist_node_next(buddy, FALSE);
	}
	return NULL;
}

void set_status(Conversation* cv, const char* status)
{
	PurpleConversation* conv = (PurpleConversation*) cv;
	if (purple_conversation_get_type(conv) == PURPLE_CONV_TYPE_IM)
	{
		PurpleBlistNode* buddy = get_buddy(conv);
		if (buddy)
		{
			purple_blist_node_set_string(buddy, "notify", status);
			char* text = g_strconcat("Notification level set to ", status, NULL);
			purple_conv_im_send_with_flags(PURPLE_CONV_IM(conv), text, PURPLE_MESSAGE_SEND);
			g_free(text);
		}
		else
		{
			purple_conv_im_send_with_flags(PURPLE_CONV_IM(conv), "Unable to change notification level", PURPLE_MESSAGE_SEND);
		}
	}
	purple_blist_schedule_save();
	purple_idle_touch();
}

/* --- Initialisation and main loop -------------------------------------- */
static int twitch_cb(gpointer data)
{
    purple_idle_touch();
    return TRUE;
}

void *lconfbot_blist_get_handle()
{
    static int handle;
    return &handle;
}

void buddy_status_changed_cb(PurpleBuddy *buddy, PurpleStatus *old_status, PurpleStatus *status)
{
	const char *name = purple_buddy_get_name(buddy);
	unsigned int stat = purple_status_is_available(status) ? STATUS_AVAILABLE : STATUS_UNAVAILABLE;
	script_notify_buddy_status_changed(name, stat);
}

void buddy_signed_on_cb(PurpleBuddy *buddy)
{
	const char *name = purple_buddy_get_name(buddy);
	script_notify_buddy_status_changed(name, STATUS_SIGNED_ON);
}

void buddy_signed_off_cb(PurpleBuddy *buddy)
{
	const char *name = purple_buddy_get_name(buddy);
	script_notify_buddy_status_changed(name, STATUS_SIGNED_OFF);
}

void im_init(void)
{
	signal(SIGPIPE, SIG_IGN);
	
	g_set_prgname("Lua Conference Bot");
	purple_util_set_user_dir(".");
	purple_debug_set_enabled(FALSE);
    
        hash = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, free_auto_recon);

	purple_core_set_ui_ops(&core_ui_ops_table);
	purple_eventloop_set_ui_ops(&eventloop_ui_ops_table);
        purple_accounts_set_ui_ops(&account_ui_ops_table);
	purple_connections_set_ui_ops(&connection_ui_ops_table);
	purple_conversations_set_ui_ops(&conversation_ui_ops_table);
	
	if (!purple_core_init(UI_ID))
	{
		fprintf(stderr, "Unable to start up libpurple.\n");
		exit(0);
	}
	
	purple_set_blist(purple_blist_new());
	purple_blist_load();
	
	{
		GList* iter = purple_accounts_get_all();

		while (iter)
		{
			PurpleAccount* account = iter->data;
			purple_account_set_enabled(account, UI_ID, TRUE);
			iter = iter->next;
		}
	}		

#if 0		
	{
		PurpleSavedStatus* available = purple_savedstatus_find(STATUS);
		if (!available)
			available = purple_savedstatus_new(STATUS, PURPLE_STATUS_AVAILABLE);
		purple_savedstatus_activate(available);
	}
#endif
        purple_signal_connect(purple_blist_get_handle(), "buddy-status-changed", lconfbot_blist_get_handle(), PURPLE_CALLBACK(buddy_status_changed_cb), NULL);
        purple_signal_connect(purple_blist_get_handle(), "buddy-signed-on", lconfbot_blist_get_handle(),  PURPLE_CALLBACK(buddy_signed_on_cb), NULL);
        purple_signal_connect(purple_blist_get_handle(), "buddy-signed-off", lconfbot_blist_get_handle(), PURPLE_CALLBACK(buddy_signed_off_cb), NULL);
	g_timeout_add(60*1000, twitch_cb, NULL);
}

static GMainLoop* loop = NULL;

void im_mainloop(void)
{
	if (!loop)
		loop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(loop);
}	

void im_terminate_mainloop(void)
{
	g_main_loop_quit(loop);
}

void im_deinit(void)
{
    purple_signals_disconnect_by_handle(lconfbot_blist_get_handle());
	purple_core_quit();
}
