-- Conversation class.
-- 
-- Each conversation is represented by an instance of this.

Conversation = {}
function Conversation.new(cv)
        local o = {}
        setmetatable(o, {__index = Conversation})
        o:_init(cv)
        return o
end

function Conversation:_init(cv)
        self._cv = cv
        self._coroutine = coroutine.create(
                function()
                        self:run()
                end
        )
        self._timeout = nil
        self._queue = {}
end

function Conversation:xmit(message)
        purple.reply(self._cv, message)
end

function Conversation:send_list(only_online)
	purple.send_list(self._cv, only_online)
end

function Conversation:set_status(status)
	purple.set_status(self._cv, status)
end

function Conversation:broadcast(name, alias, message)
	if string.find(name, '/') then
		name = string.sub(name, 1, string.find(name, '/') - 1)
	end
	local m
	if alias and #alias > 0 then
		m = alias .. ': ' .. message
	else
		m = name .. ': ' .. message
	end
	purple.broadcast(name, m, 0)
    purple.log(m)
end

function Conversation:resume(...)
        self._timeout = nil
        local f, e = coroutine.resume(self._coroutine, ...)
        if e then
                print("Error:"..e)
                debug.traceback()
        end
end

function Conversation:getmessage(delay)
        local name, alias, message, time = unpack(self._queue[1] or {})
        if message then
                table.remove(self._queue, 1)
                return name, alias, message, timestamp
        end
        
        if delay then
                if (delay ~= 0) then
                        local timer = purple.add_timeout(self._cv, delay*1000)
                        name, alias, message, timestamp = coroutine.yield()
                        if message then
                                purple.remove_timeout(self._cv, timer)
                        end
                end
        else
                name, alias, message, timestamp = coroutine.yield()
        end
        return name, alias, message, timestamp
end

function Conversation:pause(delay)
        local timer = purple.add_timeout(self._cv, delay*1000)
        
        while true do
                local name, alias, message, timestamp = coroutine.yield()
                if not message then
                        break
                end
                
                self._queue[#self._queue + 1] = {name, message, alias, timestamp}
        end
end
        
function Conversation:getqueuedmessage(delay)
        local name, alias, message = self:getmessage(0)
        if not message then
                return self:getmessage(delay)
        end
        
        message = {message}
        while true do
                local n2,a2, s = self:getmessage(0)
		name = n2
		alias = a2
                if not s then
                        break
                end
                
                message[#message+1] = s
        end
        return name, alias, table.concat(m, ".")
end

function Conversation:run()
        while true do
                local name, alias,  message = self:getqueuedmessage(10*60)
                if not message then
                        print("Conversation timed out.")
                        name, alias, message = self:getqueuedmessage()
                end
                if message then
                        message = message:gsub("%b<>", "")
			local lower = string.lower(message)
                        if lower == ")names" then
                            self:send_list(0)
			elseif lower == ")online" then
			    self:send_list(1)
			elseif lower == ")im none" then
			    self:set_status("none")
                        elseif lower == ")im status" then
			     self:set_status("status")
			elseif lower == ")im message" then
			     self:set_status("message")
			elseif lower == ")im all" then
		             self:set_status("all")
			elseif lower == ")help" then
			    self:xmit(")names      - to get list of buddies\n" ..
			              ")online     - to get online users\n" ..
				      ")im none    - don't send me any message\n" ..
				      ")im status  - send me only status messages\n" ..
				      ")im message - send me only IM messages\n" ..
				      ")im all     - send me all messages")
                        else
                            self:broadcast(name, alias, message)
                        end
                end
        end
end
